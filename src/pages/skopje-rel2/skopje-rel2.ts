import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import {AddUser} from "../../providers/add-user";
import {Vibration} from "ionic-native";

/*
  Generated class for the SkopjeRel2 page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-skopje-rel2',
  templateUrl: 'skopje-rel2.html'
})
export class SkopjeRel2Page {
  data: any = {};


  constructor(public navCtrl: NavController, public navParams: NavParams,public upload: AddUser) {
    this.data.nameofuser='';
    this.data.surnameofuser='';
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SkopjeRel2Page');
  }

  Reserve(){
    if(this.data.nameofuser && this.data.nameofuser.length >=3 && this.data.surnameofuser && this.data.surnameofuser.length >=3) {
    this.upload.addToFirebase(this.data.nameofuser,this.data.surnameofuser,"18-07-2017","12:00 AM","200","Gostilnica Dukat").subscribe(
      data=>{
        //successfully stored to firebase
        alert("Your table has been reserved");
        console.log(data);
      },err=>{
        //NOT successfully stored to firebase
        Vibration.vibrate(1000);
        alert("Not Reserved");

      });
    }else{
      Vibration.vibrate(1000);
      alert("Please enter a valid name");



    }
  }

}
