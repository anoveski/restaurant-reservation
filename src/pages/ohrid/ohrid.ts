import { Component } from '@angular/core';
import {NavController, NavParams} from 'ionic-angular';
import {OhridRel1Page} from "../ohrid-rel1/ohrid-rel1";
import {OhridRel2Page} from "../ohrid-rel2/ohrid-rel2";


/*
  Generated class for the Ohrid page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-ohrid',
  templateUrl: 'ohrid.html'
})
export class OhridPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {}

  ionViewDidLoad() {
    console.log('ionViewDidLoad OhridPage');

  }

  OhridRel1(){
    console.log("Ohrid riched 1 ");
    this.navCtrl.push(OhridRel1Page);


  }
  OhridRel2(){
    console.log("Ohrid riched 2 ");
    this.navCtrl.push(OhridRel2Page);
  }

}
