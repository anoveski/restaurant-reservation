import { Component } from '@angular/core';
import {NavController, NavParams, ModalController, ActionSheetController} from 'ionic-angular';
import {Camera, SocialSharing} from "ionic-native";
import {Storage} from "@ionic/storage";
import {ImagePage} from "../../modals/image/image";

/*
  Generated class for the Camera page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-camera',
  templateUrl: 'camera.html'
})
export class CameraPage {
  images: any = {};

  theImage: string;

  constructor(public navCtrl: NavController,
              public storage: Storage,
              public actionSheetCtrl: ActionSheetController,
              public modalCtrl: ModalController,
  ) {
    this.images = {};
    this.images.imagesArr = [];

    this.storage.get('maps.images').then( (value:any) => {

      if(!value){
        this.storage.set('maps.images', {
          count: 1,
          imagesArr: []
        });
      }else{
        this.images = value;
      }

    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CameraPage');
  }
  takePicture() {

    var cameraOptions = {
      destinationType: Camera.DestinationType.NATIVE_URI,
      sourceType: Camera.PictureSourceType.CAMERA,
      encodingType: Camera.EncodingType.JPEG,
      quality: 50,
      correctOrientation: true,
      saveToPhotoAlbum: false,
      allowEdit: false,
    };

    Camera.getPicture(cameraOptions).then((imageData) => {
      // imageData is either a base64 encoded string or a file URI
      // If it's base64:
      this.theImage= imageData;

      console.log(typeof imageData);

      this.storage.get('maps.images').then((value: any) => {

        this.images = value;

        var image = {
          id: value.count,
          src: this.theImage
        };

        this.images.imagesArr.push(image);

        this.images.count++;

        this.storage.set('maps.images', this.images);

      });

    }, (err) => {
      // Handle error
    });
  }
  openFullSizeImg(id){

    // let imgModal = this.modalCtrl.create(ImagePage, { id: id });
    let imgModal = this.modalCtrl.create(ImagePage, { id: id });
    imgModal.present();

  }
  presentActionSheet(id) {

    let actionSheet = this.actionSheetCtrl.create({
      title: 'Modify your album',
      buttons: [
        {
          text: 'Delete',
          role: 'destructive',
          handler: () => {
            // we gonna delete the photo here
            this.storage.get('maps.images').then( (value:any) => {

              this.images = value;

              this.images.imagesArr = this.images.imagesArr.filter(function(jsonObject) {
                return jsonObject.id != id;
              });

              this.storage.set('maps.images', this.images);

            });
          }
        },{
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        }
      ]
    });
    actionSheet.present();
  }





}
