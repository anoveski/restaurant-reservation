import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import {OhridPage} from "../ohrid/ohrid";
import {StrugaPage} from "../struga/struga";
import {SkopjePage} from "../skopje/skopje";
import {BitolaPage} from "../bitola/bitola";

/*
  Generated class for the BusStations page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-bus-stations',
  templateUrl: 'bus-stations.html'
})
export class BusStationsPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {}

  ionViewDidLoad() {
    console.log('ionViewDidLoad BusStationsPage');
  }

  Ohrid(){

    console.log("Riched");
    this.navCtrl.push(OhridPage);

  }
  Struga(){
    console.log("Riched");
    this.navCtrl.push(StrugaPage);
  }

  Skopje(){
    console.log("Riched");
    this.navCtrl.push(SkopjePage);
  }
  Bitola(){
    console.log("Riched");
    this.navCtrl.push(BitolaPage);
  }



}
