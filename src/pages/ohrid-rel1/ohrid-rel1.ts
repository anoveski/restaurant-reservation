import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import {AddUser} from "../../providers/add-user";
import {Vibration} from "ionic-native";

/*
  Generated class for the OhridRel1 page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-ohrid-rel1',
  templateUrl: 'ohrid-rel1.html'
})
export class OhridRel1Page {
  data: any = {};

  constructor(public navCtrl: NavController, public navParams: NavParams, public upload: AddUser) {
    this.data.nameofuser='';
    this.data.surnameofuser='';
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad OhridRel1Page');
  }

  Reserve(){

    if(this.data.nameofuser && this.data.nameofuser.length >=3 && this.data.surnameofuser && this.data.surnameofuser.length >=3) {
    this.upload.addToFirebase(this.data.nameofuser,this.data.surnameofuser,"17-07-2017","01:00 PM","100","Restaurant Dalga").subscribe(
      data=>{
        //successfully stored to firebase
        alert("Your ticket has been reserved");
        console.log(data);
      },err=>{
        //NOT successfully stored to firebase
        Vibration.vibrate(1000);
        alert("Not Reserved");

      });
    }else{
      Vibration.vibrate(1000);
      alert("Please enter a valid name");

    }


  }

}
