import { Component } from '@angular/core';

import { HomePage } from '../home/home';
import { AboutPage } from '../about/about';
import { ContactPage } from '../contact/contact';
import {BusStationsPage} from "../bus-stations/bus-stations";
import {CameraPage} from "../camera/camera";

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {
  // this tells the tabs component which Pages
  // should be each tab's root Page
  tab1Root: any = BusStationsPage;
  tab2Root: any = HomePage;
  constructor() {

  }
}
