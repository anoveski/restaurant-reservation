import { Component,ElementRef,ViewChild } from '@angular/core';

import { NavController, Platform } from 'ionic-angular';
import {Geolocation, GoogleMapsLatLng,Geocoder} from "ionic-native";

import { GoogleMap, GoogleMapsEvent } from 'ionic-native';

declare var google;



@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  watchId: any;
  @ViewChild('map') mapElement: ElementRef;
  map: any;

  constructor(public navCtrl: NavController, platform: Platform) {

    platform.ready().then(() => {

      Geolocation.getCurrentPosition({}).then((resp) => {


        let myPosition:GoogleMapsLatLng = new GoogleMapsLatLng(resp.coords.latitude, resp.coords.longitude);
        let strugastanica:GoogleMapsLatLng = new GoogleMapsLatLng(41.172912, 20.667905);
        let ohridstanica:GoogleMapsLatLng = new GoogleMapsLatLng(41.111788, 20.796980);
        let bitolastanica:GoogleMapsLatLng = new GoogleMapsLatLng(41.013223, 21.346575);
        let skopjestanica:GoogleMapsLatLng = new GoogleMapsLatLng(41.995401, 21.434584);
        let gevgelijastanica:GoogleMapsLatLng = new GoogleMapsLatLng(41.177006, 20.678563);


        let opt = {
          'backgroundColor': 'white',
          'controls': {
            'compass': true,
            'myLocationButton': true,
            'indoorPicker': true,
            'zoom': true
          },
          'gestures': {
            'scroll': true,
            'tilt': true,
            'rotate': true,
            'zoom': true
          },
          'camera': {
            'latLng': myPosition,
            'zoom': 14
          }

        };

        let map = new GoogleMap('map', opt);


        map.on(GoogleMapsEvent.MAP_READY).subscribe(() => {

          map.addMarker({
            'position': myPosition,
            'title': "You are located here"
          }).then(function (data) {
          });
          map.addMarker({
            icon: 'blue',
            'position': strugastanica,
            'title': "Restoran Aquarius",


          }).then(function (data) {
            data.addEventListener(google.maps.event.MARKER_CLICK, function () {
              alert("This is marker for Struga Autobus station");

            });

          });
          map.addMarker({

            'icon': {
              'url': 'https://d30y9cdsu7xlg0.cloudfront.net/png/74647-200.png'
            },
            'position': ohridstanica,
            'title': "Restoran Dalga",
          }).then(function (data) {
            data.addEventListener(google.maps.event.MARKER_CLICK, function () {
              alert("This is marker for Ohrid Autobus station");

            });

          });
          map.addMarker({
            'icon': {
              'url': 'https://d30y9cdsu7xlg0.cloudfront.net/png/74647-200.png'
            },
            'position': skopjestanica,
            'title': "Plaza De Toros",


          }).then(function (data) {
            data.addEventListener(google.maps.event.MARKER_CLICK, function () {
              alert("This is marker for Skopje Autobus station");

            });

          });
          map.addMarker({
            'icon': {
              'url': 'https://d30y9cdsu7xlg0.cloudfront.net/png/74647-200.png'
            },
            'position': gevgelijastanica,
            'title': "Pizzeria Angela",
            'styles' : {
              'text-align': 'center',
              'font-style': 'italic',
              'font-weight': 'bold',
              'color': 'red'
            }


          }).then(function (data) {
            data.addEventListener(google.maps.event.MARKER_CLICK, function () {
              alert("This is marker for Gevgelija Autobus station");

            });

          });

          map.addMarker({
            'icon': {
              'url': 'https://d30y9cdsu7xlg0.cloudfront.net/png/74647-200.png'
            },
            'position': bitolastanica,
            'title': "Restoran vavilon",


          }).then(function (data) {
            data.addEventListener(google.maps.event.MARKER_CLICK, function () {
              alert("This is marker for Bitola Autobus station");

            });


          });
          map.addMarker({
            'icon': {
              'url': 'https://d30y9cdsu7xlg0.cloudfront.net/png/74647-200.png'
            },
            'position': bitolastanica,
            'title': "Avtobuska stanica Bitola",


          }).then(function (data) {
            data.addEventListener(google.maps.event.MARKER_CLICK, function () {
              alert("This is marker for Bitola Autobus station");

            });


          });


        });

      });


      let watch = Geolocation.watchPosition();
      watch.subscribe((data) => {

      });

    });

  }
  ionViewDidLoad() {
    console.log('Hello MapPage Page');
  }





}


