import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import {BitolaRel1Page} from "../bitola-rel1/bitola-rel1";
import {BitolaRel2Page} from "../bitola-rel2/bitola-rel2";

/*
  Generated class for the Bitola page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-bitola',
  templateUrl: 'bitola.html'
})
export class BitolaPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {}

  ionViewDidLoad() {
    console.log('ionViewDidLoad BitolaPage');
  }
  BitolaRel1(){
    this.navCtrl.push(BitolaRel1Page);
  }
  BitolaRel2(){
    this.navCtrl.push(BitolaRel2Page);
  }

}


