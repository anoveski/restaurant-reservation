import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import {AddUser} from "../../providers/add-user";
import {Vibration} from "ionic-native";

/*
  Generated class for the OhridRel2 page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-ohrid-rel2',
  templateUrl: 'ohrid-rel2.html'
})
export class OhridRel2Page {
  data: any = {};


  constructor(public navCtrl: NavController, public navParams: NavParams,public upload: AddUser) {
    this.data.nameofuser='';
    this.data.surnameofuser='';
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad OhridRel2Page');
  }
  Reserve(){
    console.log("Riched");
    if(this.data.nameofuser && this.data.nameofuser.length >=3 && this.data.surnameofuser && this.data.surnameofuser.length >=3) {
    this.upload.addToFirebase(this.data.nameofuser,this.data.surnameofuser,"15-07-2017","10:00 AM","150","Restaurant Belvedere").subscribe(
      data=>{
        //successfully stored to firebase
        alert("Your ticket has been reserved");
        console.log(data);
      },err=>{
        //NOT successfully stored to firebase
        Vibration.vibrate(1000);
        alert("Not Reserved");

      });

  }else{
      Vibration.vibrate(1000);
  alert("Please enter a valid name");

}

  }

}
