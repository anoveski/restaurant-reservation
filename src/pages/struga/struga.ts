import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import {StrugaRel1Page} from "../struga-rel1/struga-rel1";
import {StrugaRel2Page} from "../struga-rel2/struga-rel2";

/*
  Generated class for the Struga page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-struga',
  templateUrl: 'struga.html'
})
export class StrugaPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {}

  ionViewDidLoad() {
    console.log('ionViewDidLoad StrugaPage');
  }
  StrugaRel1(){
    console.log("Ohrid riched 1 ");
    this.navCtrl.push(StrugaRel1Page);


  }
  StrugaRel2(){
    console.log("Ohrid riched 2 ");
    this.navCtrl.push(StrugaRel2Page);
  }

}
