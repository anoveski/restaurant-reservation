import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import {SkopjeRel1Page} from "../skopje-rel1/skopje-rel1";
import {SkopjeRel2Page} from "../skopje-rel2/skopje-rel2";


/*
  Generated class for the Skopje page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-skopje',
  templateUrl: 'skopje.html'
})
export class SkopjePage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {}

  ionViewDidLoad() {
    console.log('ionViewDidLoad SkopjePage');
  }

  SkopjeRel1(){
    console.log("Ohrid riched 1 ");
    this.navCtrl.push(SkopjeRel1Page);


  }
  SkopjeRel2(){
    console.log("Ohrid riched 2 ");
    this.navCtrl.push(SkopjeRel2Page);
  }

}
