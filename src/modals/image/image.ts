import {Component, ViewChild, ViewEncapsulation} from '@angular/core';
import {NavController, ViewController, NavParams, Content, Slides} from 'ionic-angular';
import {Storage} from "@ionic/storage";
import {Subject} from "rxjs";

/*
 Generated class for the Image page.

 See http://ionicframework.com/docs/v2/components/#navigation for more info on
 Ionic pages and navigation.
 */
@Component({
   //encapsulation: ViewEncapsulation.None,
    selector: 'page-image',
    templateUrl: 'image.html'
})
export class ImagePage {



  images: any = {};


    imageID: number;
    image: string = '';

    constructor(public navPar: NavParams, public viewCtrl: ViewController, public storage:Storage,params: NavParams) {




        this.imageID = this.navPar.get('id');

        this.storage.get('maps.images').then( (value:any) => {

            this.images = value;

            for(var i=0; i< this.images.imagesArr.length; i++){

                if(this.images.imagesArr[i].id == this.imageID){
                    this.image = this.images.imagesArr[i].src;
                }

            }

        });


    }




    ionViewDidLoad() {
        console.log('Hello ImagePage Page');
    }

    dismiss() {
        this.viewCtrl.dismiss();
    }

    changeImg(e){
        if(e.direction == 4){

            for(var i=0; i< this.images.imagesArr.length; i++){

                if(this.images.imagesArr[i].id == this.imageID){

                    if(i-1 >= 0){
                        this.image = this.images.imagesArr[i-1].src;
                        this.imageID = this.images.imagesArr[i-1].id;
                        break;
                    }
                }
            }
        }
        if(e.direction == 2){
            for(var i=0; i< this.images.imagesArr.length; i++){

                if(this.images.imagesArr[i].id == this.imageID){

                    if(this.images.imagesArr.length > i+1){
                        this.image = this.images.imagesArr[i+1].src;
                        this.imageID = this.images.imagesArr[i+1].id;
                        break;
                    }
                }
            }
        }
    }




}
