import { NgModule, ErrorHandler } from '@angular/core';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { TabsPage } from '../pages/tabs/tabs';
import {AddUser} from "../providers/add-user";
import {BusStationsPage} from "../pages/bus-stations/bus-stations";
import {OhridPage} from "../pages/ohrid/ohrid";
import {OhridRel1Page} from "../pages/ohrid-rel1/ohrid-rel1";
import {OhridRel2Page} from "../pages/ohrid-rel2/ohrid-rel2";
import {StrugaPage} from"../pages/struga/struga";
import {StrugaRel1Page} from"../pages/struga-rel1/struga-rel1";
import {StrugaRel2Page} from"../pages/struga-rel2/struga-rel2";
import {SkopjePage} from "../pages/skopje/skopje";
import {SkopjeRel1Page} from "../pages/skopje-rel1/skopje-rel1";
import {SkopjeRel2Page} from "../pages/skopje-rel2/skopje-rel2";
import {BitolaPage} from "../pages/bitola/bitola";
import {BitolaRel1Page} from "../pages/bitola-rel1/bitola-rel1";
import {BitolaRel2Page} from "../pages/bitola-rel2/bitola-rel2";
import {CameraPage} from "../pages/camera/camera";
import {Storage} from "@ionic/storage";
import {ImagePage} from "../modals/image/image";







@NgModule({
  declarations: [
    MyApp,
    HomePage,
    TabsPage,
    BusStationsPage,
    OhridPage,
    OhridRel1Page,
    OhridRel2Page,
    StrugaPage,
    StrugaRel1Page,
    StrugaRel2Page,
    SkopjePage,
    SkopjeRel1Page,
    SkopjeRel2Page,
    BitolaPage,
    BitolaRel1Page,
    BitolaRel2Page,
    CameraPage,
    ImagePage



  ],
  imports: [
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    TabsPage,
    BusStationsPage,
    OhridPage,
    OhridRel1Page,
    OhridRel2Page,
    StrugaPage,
    StrugaRel1Page,
    StrugaRel2Page,
    SkopjePage,
    SkopjeRel1Page,
    SkopjeRel2Page,
    BitolaPage,
    BitolaRel1Page,
    BitolaRel2Page,
    CameraPage,
    ImagePage



  ],

   providers: [{provide: ErrorHandler, useClass: IonicErrorHandler}, AddUser, Storage]
})
export class AppModule {}
