import {Injectable} from '@angular/core';
import {Http} from '@angular/http';
import 'rxjs/add/operator/map';
import {Observable} from "rxjs";
import {Response, RequestOptions, Headers} from "@angular/http";
import {Events} from "ionic-angular";


/*
 Generated class for the AddUser provider.

 See https://angular.io/docs/ts/latest/guide/dependency-injection.html
 for more info on providers and Angular 2 DI.
 */
@Injectable()
export class AddUser {

  constructor(public http: Http, public events: Events) {
    console.log('Hello AddUser Provider');
    //How to use this provider:
    //to add a new ticket

    /*this.addToFirebase("John","Doe","01-02-2017","3:40","10").subscribe(
     data=>{
     //successfully stored to firebase
     console.log(data);
     },err=>{
     //NOT successfully stored to firebase
     console.log(err);
     });*/


    //to retrieve a ticket
    //you must specify name and surname

    /*this.retrieveFromFirebase("John","Doe").subscribe(
     data => {
     console.log(data);
     },err=>{
     console.log(err);
     });
     */
  }


  addToFirebase(name, surname, date, time, price, restaurant): Observable<Response> {
    let body = {
      name: name,
      surname: surname,
      date: date,
      time: time,
      price: price,
      restaurant: restaurant
    };
    let headers = new Headers({'Content-Type': 'application/json'}); // ... Set content type to JSON
    let options = new RequestOptions({headers: headers}); // Create a request option
    let url = 'https://restaurantreservation-f5ffd.firebaseio.com/tickets' + name.toLowerCase() + "-" + surname.toLowerCase() + '.json';
    return this.http.put(url, body, options) // ...using post request
      .map((res: Response) => res.json()) // ...and calling .json() on the response to return data
      .catch((error: any) => Observable.throw(error.json().error || 'Server error')); //...errors if any

  }

  retrieveFromFirebase(name, surname): Observable<Response> {

    return this.http.get(
      'https://restaurantreservation-f5ffd.firebaseio.com/tickets/' + name.toLowerCase() + "-" + surname.toLowerCase() + '.json'
    ).map(res => res.json());
  }

}
